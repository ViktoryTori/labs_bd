from . import db

class BaseModel:

	table = ''

	def __init__(self, table):
		self.table = table

	def delByID(self, id):
		db.delete(self.table, where = "WHERE `id` = '" + str(id) + "'")
		return 1
# 
class Index (BaseModel):

	def __init__(self):
		pass

	def getPageData(self):
		return []

	def fillDataBaseWithJSONFile(self, table, fieldsValues):
		return db.insert('`' + table + '`', fieldsValues)
# 
class Comments (BaseModel):

	def updComment(self, request):
		author = request.GET['comment-author']
		comment = request.GET['comment-text']
		id = request.GET['id']
		db.update(self.table, {'comment': comment, 'author': author}, " WHERE `id` = '" + id + "'")
		return 1

	def addNewComment(self, request):
		author = request.GET['comment-author']
		comment = request.GET['comment-text']
		article = request.GET['article']
		art = db.select('`articles`', '*', " WHERE `title` = '" + article + "'")
		if len(art) == 0:
			return False
		return db.insert(self.table, {'author': author, 'comment': comment, 'article_id': art[0]['id']})

	def getAllComments(self):
		return {'name': self.table,
				'table': db.select('`comments`, `articles`',
									'`comment`, `comments`.`id`, `author`, `articles`.`title` as `article` ',
									' WHERE `comments`.`article_id` = `articles`.`id`')}
#
class Articles (BaseModel):

	def updArticle(self, request):
		article = request.GET['article-title']
		text = request.GET['article-text']
		id = request.GET['id']
		db.update(self.table, {'title': article, 'text': text}, " WHERE `id` = '" + id + "'")
		return 1


	def addNewArticle(self, request):
		title = request.GET['article-title']
		text = request.GET['article-text']
		category = request.GET['category']
		if len(db.select(self.table, '*', " WHERE `title` = '" + title + "' OR `text` = '" + text + "'")) > 0:
			return False
		cat = db.select('`categories`', '*', " WHERE `title` = '" + category + "'")
		if len(cat) == 0:
			return False
		return db.insert(self.table, {'title': title, 'text': text, 'category_id': cat[0]['id']})

	def getAllArticles(self):
		return {'name': self.table,
				'table': db.select('`categories`, `articles`',
									'`articles`.`id`, `articles`.`text`, `articles`.`title` as article, `categories`.`title` as `category` ',
									' WHERE `articles`.`category_id` = `categories`.`id`')}
#
class Categories (BaseModel):

	def updCategory(self, request):
		category = request.GET['category-title']
		description = request.GET['category-description']
		id = request.GET['id']
		db.update(self.table, {'title': category, 'description': description}, " WHERE `id` = '" + id + "'")
		return 1

	def addNewCategory(self, request):
		title = request.GET['category-title']
		description = request.GET['category-description']
		if len(db.select(self.table, '*', " WHERE `title` = '" + title + "' OR `description` = '" + description + "'")) > 0:
			return False
		return db.insert(self.table, {'title': title, 'description': description})

	def getAllCategories(self):
		return {'name': self.table, 
				'table': db.select('`categories`', '`id`, `title` as `category`, `description`')}
