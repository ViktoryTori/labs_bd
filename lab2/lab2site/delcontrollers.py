from .views import View
from .models import Index
from .models import Comments
from .models import Articles
from .models import Categories

def categories(request):
	model = Categories('categories')
	model.delByID(request.GET['id'])
	return View('categories.html', request, model.getAllCategories()).renderPage()

def articles(request):
	model = Articles('articles')
	model.delByID(request.GET['id'])
	return View('articles.html', request, model.getAllArticles()).renderPage()

def comments(request):
	model = Comments('comments')
	model.delByID(request.GET['id'])
	return View('comments.html', request, model.getAllComments()).renderPage()
