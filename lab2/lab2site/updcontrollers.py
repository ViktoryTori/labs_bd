from django.http import HttpResponse
from .models import Index
from .models import Comments
from .models import Articles
from .models import Categories

def categories(request):
	model = Categories('categories')
	if model.updCategory(request):
		return HttpResponse(1)
	return HttpResponse(0)

def articles(request):
	model = Articles('articles')
	if model.updArticle(request):
		return HttpResponse(1)
	return HttpResponse(0)

def comments(request):
	model = Comments('comments')
	if model.updComment(request):
		return HttpResponse(1)
	return HttpResponse(0)