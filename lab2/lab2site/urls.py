from django.conf.urls import url
from . import pagecontrollers
from . import addcontrollers
from . import delcontrollers
from . import updcontrollers

urlpatterns = [

	url(r'^article-upd', updcontrollers.articles),
	url(r'^comment-upd', updcontrollers.comments),
 	url(r'^category-upd', updcontrollers.categories),

	url(r'category-add', addcontrollers.categories),
	url(r'article-add', addcontrollers.articles),
	url(r'comment-add', addcontrollers.comments),
	url(r'fill-db-from-json', addcontrollers.fillDataBaseFromJSON),

	url(r'category-del', delcontrollers.categories),
	url(r'article-del', delcontrollers.articles),
	url(r'comment-del', delcontrollers.comments),

	url(r'^$', pagecontrollers.index),
	url(r'comments', pagecontrollers.comments),
	url(r'articles', pagecontrollers.articles),
	url(r'categories', pagecontrollers.categories)

]
