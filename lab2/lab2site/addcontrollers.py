from django.http import HttpResponse
from .models import Index
from .models import Comments
from .models import Articles
from .models import Categories
import json

def fillDataBaseFromJSON(request):
	with open('lab2site/static/db.json') as dataFile:
		data = json.load(dataFile)
		model = Index()
		for item in data:
			for element in data[item]:
				if model.fillDataBaseWithJSONFile(item, element) <= 0:
					return HttpResponse(0)
	return HttpResponse(1)

def categories(request):
	model = Categories('categories')
	if model.addNewCategory(request):
		return HttpResponse(1)
	return HttpResponse(0)

def articles(request):
	model = Articles('articles')
	if model.addNewArticle(request):
		return HttpResponse(1)
	return HttpResponse(0)

def comments(request):
	model = Comments('comments')
	if model.addNewComment(request):
		return HttpResponse(1)
	return HttpResponse(0)