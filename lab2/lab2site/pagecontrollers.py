from .views import View
from .models import Index
from .models import Comments
from .models import Articles
from .models import Categories

def index(request):
	model = Index()
	return View('index.html', request, model.getPageData()).renderPage()

def comments(request):
	model = Comments('comments')
	return View('comments.html', request, model.getAllComments()).renderPage()

def articles(request):
	model = Articles('articles')
	return View('articles.html', request, model.getAllArticles()).renderPage()

def categories(request):
	model = Categories('categories')
	return View('categories.html', request, model.getAllCategories()).renderPage()