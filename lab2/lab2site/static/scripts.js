$(document).ready(function () {

	/* add section */

	$('#fill_with_json').click(function (e) {
		e.preventDefault();
		sendAjax('/fill-db-from-json', '', function (answer) {
			alert(answer);
		});
	});

	$('.add-category').click(function (e) {
		e.preventDefault();
		sendAjax('/category-add', $('#add-category').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/categories/', 'category was not added');
		});
	});

	$('.add-comment').click(function (e) {
		e.preventDefault();
		sendAjax('/comment-add', $('#add-comment').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/comments/', 'comment was not added');
		});
	});


	$('.add-article').click(function (e) {
		e.preventDefault();	
		sendAjax('/article-add', $('#add-article').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/articles/', 'article was not added');
		});
	});

	/* end add section*/

	/* upd section */

	$('.update-comments').click(function (e) {
		e.preventDefault();	
		setCommentUpdForm($(this).parent().parent().attr('class'));
	});

	$('.update-articles').click(function (e) {
		e.preventDefault();	
		setArticleUpdForm($(this).parent().parent().attr('class'));
	});

	$('.update-categories').click(function (e) {
		e.preventDefault();	
		setCategoryUpdForm($(this).parent().parent().attr('class'));
	});

	$('.upd-article').click(function (e) {
		e.preventDefault();	
		sendAjax('/article-upd', $('#upd-article').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/articles/', 'article was not updated');
		});
	});

	$('.upd-comment').click(function (e) {
		e.preventDefault();	
		sendAjax('/comment-upd', $('#upd-comment').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/comments/', 'category was not updated');
		});
	});

	$('.upd-category').click(function (e) {
		e.preventDefault();	
		sendAjax('/category-upd', $('#upd-category').serialize(), function (answer) {
			setResultOfAdding(answer, 'http://localhost:8000/categories/', 'category was not updated');
		});
	});

	/* end upd section */

});

function setResultOfAdding(res, link, message) {
	if (res == 1)
		$(location).attr("href", link);
	else
		alert(message);
}

function sendAjax(link, data, callback) {
	$.ajax({
		url: link,
		type: 'get',
		data: data,
		success: callback
	});
}

function setCommentUpdForm(index) {
	var comment = $('.' + index + ' > .comment').text();
	var author = $('.' + index + ' > .author').text();
	$('#upd-comment > .comment-text').val(comment);
	$('#upd-comment > .comment-author').val(author);
	$('#upd-comment').children('.id').remove();
	$('#upd-comment').append('<input name="id" class="id" type="hidden" value="' + index + '">');
	$('#upd-comment').show();
}

function setArticleUpdForm(index) {
	var article = $('.' + index + ' > .article').text();
	var text = $('.' + index + ' > .text').text();
	$('#upd-article > .article-title').val(article);
	$('#upd-article > .article-text').val(text);
	$('#upd-article').children('.id').remove();
	$('#upd-article').append('<input name="id" class="id" type="hidden" value="' + index + '">');
	$('#upd-article').show();
}

function setCategoryUpdForm(index) {
	var category = $('.' + index + ' > .category').text();
	var text = $('.' + index + ' > .description').text();
	$('#upd-category > .category-description').val(text);
	$('#upd-category > .category-title').val(category);
	$('#upd-category').children('.id').remove();
	$('#upd-category').append('<input name="id" class="id" type="hidden" value="' + index + '">');
	$('#upd-category').show();
}